﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.UploadButton = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.MapBox = New System.Windows.Forms.PictureBox()
        Me.FileBox = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.NewMarker = New System.Windows.Forms.Button()
        Me.PartyButton = New System.Windows.Forms.Button()
        Me.JournalButton = New System.Windows.Forms.Button()
        Me.RelationshipButton = New System.Windows.Forms.Button()
        CType(Me.MapBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UploadButton
        '
        Me.UploadButton.Location = New System.Drawing.Point(378, 5)
        Me.UploadButton.Name = "UploadButton"
        Me.UploadButton.Size = New System.Drawing.Size(85, 27)
        Me.UploadButton.TabIndex = 0
        Me.UploadButton.Text = "upload"
        Me.UploadButton.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'MapBox
        '
        Me.MapBox.Enabled = False
        Me.MapBox.Location = New System.Drawing.Point(0, 1)
        Me.MapBox.Name = "MapBox"
        Me.MapBox.Size = New System.Drawing.Size(315, 197)
        Me.MapBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.MapBox.TabIndex = 1
        Me.MapBox.TabStop = False
        '
        'FileBox
        '
        Me.FileBox.Location = New System.Drawing.Point(13, 12)
        Me.FileBox.Name = "FileBox"
        Me.FileBox.Size = New System.Drawing.Size(348, 20)
        Me.FileBox.TabIndex = 2
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(469, 7)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "OK"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'NewMarker
        '
        Me.NewMarker.Location = New System.Drawing.Point(550, 7)
        Me.NewMarker.Name = "NewMarker"
        Me.NewMarker.Size = New System.Drawing.Size(75, 23)
        Me.NewMarker.TabIndex = 5
        Me.NewMarker.Text = "NewMarker"
        Me.NewMarker.UseVisualStyleBackColor = True
        '
        'PartyButton
        '
        Me.PartyButton.Location = New System.Drawing.Point(755, 7)
        Me.PartyButton.Name = "PartyButton"
        Me.PartyButton.Size = New System.Drawing.Size(75, 23)
        Me.PartyButton.TabIndex = 6
        Me.PartyButton.Text = "Party"
        Me.PartyButton.UseVisualStyleBackColor = True
        '
        'JournalButton
        '
        Me.JournalButton.Location = New System.Drawing.Point(657, 7)
        Me.JournalButton.Name = "JournalButton"
        Me.JournalButton.Size = New System.Drawing.Size(75, 23)
        Me.JournalButton.TabIndex = 7
        Me.JournalButton.Text = "Journal"
        Me.JournalButton.UseVisualStyleBackColor = True
        '
        'RelationshipButton
        '
        Me.RelationshipButton.Location = New System.Drawing.Point(755, 36)
        Me.RelationshipButton.Name = "RelationshipButton"
        Me.RelationshipButton.Size = New System.Drawing.Size(75, 23)
        Me.RelationshipButton.TabIndex = 8
        Me.RelationshipButton.Text = "Relationships"
        Me.RelationshipButton.UseVisualStyleBackColor = True
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(842, 333)
        Me.Controls.Add(Me.RelationshipButton)
        Me.Controls.Add(Me.JournalButton)
        Me.Controls.Add(Me.PartyButton)
        Me.Controls.Add(Me.NewMarker)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.FileBox)
        Me.Controls.Add(Me.UploadButton)
        Me.Controls.Add(Me.MapBox)
        Me.Name = "Form2"
        Me.Text = "Form2"
        CType(Me.MapBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UploadButton As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents MapBox As System.Windows.Forms.PictureBox
    Friend WithEvents FileBox As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents NewMarker As System.Windows.Forms.Button
    Friend WithEvents PartyButton As System.Windows.Forms.Button
    Friend WithEvents JournalButton As System.Windows.Forms.Button
    Friend WithEvents RelationshipButton As System.Windows.Forms.Button
End Class
