﻿Public Class Form3
    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles HPUp1.Click, HPUp2.Click, HPUp3.Click, HPUp4.Click
        'checks the sender of the up and down commands for the health bars, ensuring that the right one is clicked and the right bar is modified
        If sender.name = "HPUp1" Then
            If PCBar1.Value <> 100 Then
                PCBar1.Value += 1
            End If
        ElseIf sender.name = "HPUp2" Then
            If PCBar2.Value <> 100 Then
                PCBar2.Value += 1
            End If
        ElseIf sender.name = "HPUp3" Then
            If PCBar3.Value <> 100 Then
                PCBar3.Value += 1
            End If
        ElseIf sender.name = "HPUp4" Then
            If PCBar4.Value <> 100 Then
                PCBar4.Value += 1
            End If
        End If
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles HPDown1.Click, HPDown2.Click, HPDown3.Click, HPDown4.Click
        'checks the sender of the up and down commands for the health bars, ensuring that the right one is clicked and the right bar is modified
        If sender.name = "HPDown1" Then
            If PCBar1.Value <> 0 Then
                PCBar1.Value -= 1
            End If
        End If
        If sender.name = "HPDown2" Then
            If PCBar1.Value <> 0 Then
                PCBar2.Value -= 1
            End If
        End If
        If sender.name = "HPDown3" Then
            If PCBar2.Value <> 0 Then
                PCBar3.Value -= 1
            End If
        End If
        If sender.name = "HPDown4" Then
            If PCBar1.Value <> 0 Then
                PCBar4.Value -= 1
            End If
        End If
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles JournalButton.Click
        Me.Hide()
        Form1.Show()
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles MapButton.Click
        Me.Hide()
        Form2.Show()
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles RelationshipButton.Click
        Me.Hide()
        Form4.Show()
    End Sub
End Class