﻿Public Class Form2
    Dim drag As Boolean
    Dim newname As String
    Dim a As Integer
    Dim target As Object
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles UploadButton.Click
        'opens the filde dialog window
        OpenFileDialog1.Title = "Please Select a File"
        OpenFileDialog1.InitialDirectory = "C:temp"
        OpenFileDialog1.ShowDialog()
    End Sub

    Private Sub OpenFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        'opens a file reader and allows for the reading in of the file name of a jpeg, which is then used for the background
        Dim strm As System.IO.Stream
        strm = OpenFileDialog1.OpenFile()
        FileBox.Text = OpenFileDialog1.FileName.ToString()
        If Not (strm Is Nothing) Then
            'insert code to read the file data
            strm.Close()
            MessageBox.Show("file uploaded")
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'forces the picturebox to be sent to the back
        MapBox.Image = Image.FromFile(FileBox.Text)
        MapBox.SendToBack()
    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles Me.Load
        'ensures that the map form fills the client's window
        Me.WindowState = FormWindowState.Maximized
        MapBox.Size = Me.Size
    End Sub

    Private Sub Form2_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown
        'Button3.Location = New Point(MousePosition.X, MousePosition.Y)
        drag = True
    End Sub
    Private Sub Form2_MouseUp(sender As Object, e As MouseEventArgs) Handles Me.MouseUp
        'ensures that moving of objects can only occur when the mouse is held down
        drag = False
    End Sub

    Private Sub NewMarker_Click(sender As Object, e As EventArgs) Handles NewMarker.Click
        'creates the label and gives it a dynamic name based on the number of pre-existing labels
        newname = InputBox("Enter the name of the new marker", "newmarker", "")
        Dim kek As New Label
        kek.Name = "Marker" & a
        'sets the other features for the label
        kek.Text = newname
        kek.Size = New Size(100, 100)
        kek.Font = New Drawing.Font("Copperplate Gothic", 12)
        kek.Location = New Point(100, 100)
        kek.AutoSize = True
        Me.Controls.Add(kek)
        kek.BringToFront()
        a += 1
    End Sub
    Private Sub Form2_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
        'checks whether the user is currently dragging
        If drag = True Then
            'cycles through all of the markers existing
            For i = 1 To a
                'checks if the user is inside the boundary of the markers
                If Me.Controls("Marker" & (i - 1)).Bounds.Contains(Form.MousePosition) Then
                    If drag = True Then
                        'modifies the position of the label to be equal to the mouse's position
                        Me.Controls("Marker" & (i - 1)).Left += (MousePosition.X - Me.Controls("Marker" & (i - 1)).Left - 12)
                        Me.Controls("Marker" & (i - 1)).Top += (MousePosition.Y - Me.Controls("Marker" & (i - 1)).Top - 12)
                        Refresh()
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles JournalButton.Click
        Me.Hide()
        Form1.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles PartyButton.Click
        Me.Hide()
        Form3.Show()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles RelationshipButton.Click
        Me.Hide()
        Form4.Show()
    End Sub
End Class