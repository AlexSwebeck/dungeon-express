﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form4
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.NewMarker = New System.Windows.Forms.Button()
        Me.RelType = New System.Windows.Forms.ComboBox()
        Me.JournalButton = New System.Windows.Forms.Button()
        Me.MapButton = New System.Windows.Forms.Button()
        Me.PartyButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'NewMarker
        '
        Me.NewMarker.Location = New System.Drawing.Point(524, 27)
        Me.NewMarker.Name = "NewMarker"
        Me.NewMarker.Size = New System.Drawing.Size(109, 23)
        Me.NewMarker.TabIndex = 0
        Me.NewMarker.Text = "Create Marker"
        Me.NewMarker.UseVisualStyleBackColor = True
        '
        'RelType
        '
        Me.RelType.FormattingEnabled = True
        Me.RelType.Items.AddRange(New Object() {"Enemy", "Friend", "Memer"})
        Me.RelType.Location = New System.Drawing.Point(397, 27)
        Me.RelType.Name = "RelType"
        Me.RelType.Size = New System.Drawing.Size(121, 21)
        Me.RelType.TabIndex = 1
        Me.RelType.Text = "Friend"
        '
        'JournalButton
        '
        Me.JournalButton.Location = New System.Drawing.Point(707, 41)
        Me.JournalButton.Name = "JournalButton"
        Me.JournalButton.Size = New System.Drawing.Size(75, 23)
        Me.JournalButton.TabIndex = 2
        Me.JournalButton.Text = "Journal"
        Me.JournalButton.UseVisualStyleBackColor = True
        '
        'MapButton
        '
        Me.MapButton.Location = New System.Drawing.Point(707, 70)
        Me.MapButton.Name = "MapButton"
        Me.MapButton.Size = New System.Drawing.Size(75, 23)
        Me.MapButton.TabIndex = 3
        Me.MapButton.Text = "Map"
        Me.MapButton.UseVisualStyleBackColor = True
        '
        'PartyButton
        '
        Me.PartyButton.Location = New System.Drawing.Point(707, 12)
        Me.PartyButton.Name = "PartyButton"
        Me.PartyButton.Size = New System.Drawing.Size(75, 23)
        Me.PartyButton.TabIndex = 4
        Me.PartyButton.Text = "party"
        Me.PartyButton.UseVisualStyleBackColor = True
        '
        'Form4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 373)
        Me.Controls.Add(Me.PartyButton)
        Me.Controls.Add(Me.MapButton)
        Me.Controls.Add(Me.JournalButton)
        Me.Controls.Add(Me.RelType)
        Me.Controls.Add(Me.NewMarker)
        Me.Name = "Form4"
        Me.Text = "Form4"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewMarker As System.Windows.Forms.Button
    Friend WithEvents RelType As System.Windows.Forms.ComboBox
    Friend WithEvents JournalButton As System.Windows.Forms.Button
    Friend WithEvents MapButton As System.Windows.Forms.Button
    Friend WithEvents PartyButton As System.Windows.Forms.Button
End Class
