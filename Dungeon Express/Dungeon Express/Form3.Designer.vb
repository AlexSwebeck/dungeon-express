﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form3
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PCBox1 = New System.Windows.Forms.GroupBox()
        Me.HPUp1 = New System.Windows.Forms.Button()
        Me.HPDown1 = New System.Windows.Forms.Button()
        Me.PCT12 = New System.Windows.Forms.TextBox()
        Me.PCT11 = New System.Windows.Forms.TextBox()
        Me.PCL12 = New System.Windows.Forms.Label()
        Me.PCL11 = New System.Windows.Forms.Label()
        Me.PHP1 = New System.Windows.Forms.Label()
        Me.PCBar1 = New System.Windows.Forms.ProgressBar()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.HPUp2 = New System.Windows.Forms.Button()
        Me.HPDown2 = New System.Windows.Forms.Button()
        Me.PCT22 = New System.Windows.Forms.TextBox()
        Me.PCT21 = New System.Windows.Forms.TextBox()
        Me.PCL22 = New System.Windows.Forms.Label()
        Me.PCL21 = New System.Windows.Forms.Label()
        Me.PHP2 = New System.Windows.Forms.Label()
        Me.PCBar2 = New System.Windows.Forms.ProgressBar()
        Me.PCBox3 = New System.Windows.Forms.GroupBox()
        Me.HPUp3 = New System.Windows.Forms.Button()
        Me.HPDown3 = New System.Windows.Forms.Button()
        Me.PCT32 = New System.Windows.Forms.TextBox()
        Me.PCT31 = New System.Windows.Forms.TextBox()
        Me.PCL32 = New System.Windows.Forms.Label()
        Me.PCL31 = New System.Windows.Forms.Label()
        Me.PHP3 = New System.Windows.Forms.Label()
        Me.PCBar3 = New System.Windows.Forms.ProgressBar()
        Me.PCBox4 = New System.Windows.Forms.GroupBox()
        Me.HPUp4 = New System.Windows.Forms.Button()
        Me.HPDown4 = New System.Windows.Forms.Button()
        Me.PCT42 = New System.Windows.Forms.TextBox()
        Me.PCT41 = New System.Windows.Forms.TextBox()
        Me.PCL42 = New System.Windows.Forms.Label()
        Me.PCL41 = New System.Windows.Forms.Label()
        Me.PHP4 = New System.Windows.Forms.Label()
        Me.PCBar4 = New System.Windows.Forms.ProgressBar()
        Me.JournalButton = New System.Windows.Forms.Button()
        Me.MapButton = New System.Windows.Forms.Button()
        Me.RelationshipButton = New System.Windows.Forms.Button()
        Me.PCBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.PCBox3.SuspendLayout()
        Me.PCBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'PCBox1
        '
        Me.PCBox1.Controls.Add(Me.HPUp1)
        Me.PCBox1.Controls.Add(Me.HPDown1)
        Me.PCBox1.Controls.Add(Me.PCT12)
        Me.PCBox1.Controls.Add(Me.PCT11)
        Me.PCBox1.Controls.Add(Me.PCL12)
        Me.PCBox1.Controls.Add(Me.PCL11)
        Me.PCBox1.Controls.Add(Me.PHP1)
        Me.PCBox1.Controls.Add(Me.PCBar1)
        Me.PCBox1.Location = New System.Drawing.Point(2, 2)
        Me.PCBox1.Name = "PCBox1"
        Me.PCBox1.Size = New System.Drawing.Size(303, 165)
        Me.PCBox1.TabIndex = 0
        Me.PCBox1.TabStop = False
        Me.PCBox1.Text = "PC1"
        '
        'HPUp1
        '
        Me.HPUp1.Location = New System.Drawing.Point(285, 39)
        Me.HPUp1.Name = "HPUp1"
        Me.HPUp1.Size = New System.Drawing.Size(13, 23)
        Me.HPUp1.TabIndex = 8
        Me.HPUp1.Text = "^"
        Me.HPUp1.UseVisualStyleBackColor = True
        '
        'HPDown1
        '
        Me.HPDown1.Location = New System.Drawing.Point(2, 40)
        Me.HPDown1.Name = "HPDown1"
        Me.HPDown1.Size = New System.Drawing.Size(13, 23)
        Me.HPDown1.TabIndex = 7
        Me.HPDown1.Text = "v"
        Me.HPDown1.UseVisualStyleBackColor = True
        '
        'PCT12
        '
        Me.PCT12.Location = New System.Drawing.Point(151, 94)
        Me.PCT12.Multiline = True
        Me.PCT12.Name = "PCT12"
        Me.PCT12.Size = New System.Drawing.Size(146, 65)
        Me.PCT12.TabIndex = 6
        '
        'PCT11
        '
        Me.PCT11.Location = New System.Drawing.Point(6, 94)
        Me.PCT11.Multiline = True
        Me.PCT11.Name = "PCT11"
        Me.PCT11.Size = New System.Drawing.Size(139, 65)
        Me.PCT11.TabIndex = 5
        '
        'PCL12
        '
        Me.PCL12.AutoSize = True
        Me.PCL12.Location = New System.Drawing.Point(148, 77)
        Me.PCL12.Name = "PCL12"
        Me.PCL12.Size = New System.Drawing.Size(51, 13)
        Me.PCL12.TabIndex = 4
        Me.PCL12.Text = "Inventory"
        '
        'PCL11
        '
        Me.PCL11.AutoSize = True
        Me.PCL11.Location = New System.Drawing.Point(2, 77)
        Me.PCL11.Name = "PCL11"
        Me.PCL11.Size = New System.Drawing.Size(72, 13)
        Me.PCL11.TabIndex = 3
        Me.PCL11.Text = "Status effects"
        '
        'PHP1
        '
        Me.PHP1.AutoSize = True
        Me.PHP1.Location = New System.Drawing.Point(7, 16)
        Me.PHP1.Name = "PHP1"
        Me.PHP1.Size = New System.Drawing.Size(38, 13)
        Me.PHP1.TabIndex = 2
        Me.PHP1.Text = "Health"
        '
        'PCBar1
        '
        Me.PCBar1.Location = New System.Drawing.Point(16, 41)
        Me.PCBar1.Name = "PCBar1"
        Me.PCBar1.Size = New System.Drawing.Size(267, 20)
        Me.PCBar1.TabIndex = 1
        Me.PCBar1.Value = 100
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.HPUp2)
        Me.GroupBox2.Controls.Add(Me.HPDown2)
        Me.GroupBox2.Controls.Add(Me.PCT22)
        Me.GroupBox2.Controls.Add(Me.PCT21)
        Me.GroupBox2.Controls.Add(Me.PCL22)
        Me.GroupBox2.Controls.Add(Me.PCL21)
        Me.GroupBox2.Controls.Add(Me.PHP2)
        Me.GroupBox2.Controls.Add(Me.PCBar2)
        Me.GroupBox2.Location = New System.Drawing.Point(2, 195)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(303, 165)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "PC2"
        '
        'HPUp2
        '
        Me.HPUp2.Location = New System.Drawing.Point(285, 39)
        Me.HPUp2.Name = "HPUp2"
        Me.HPUp2.Size = New System.Drawing.Size(13, 23)
        Me.HPUp2.TabIndex = 8
        Me.HPUp2.Text = "^"
        Me.HPUp2.UseVisualStyleBackColor = True
        '
        'HPDown2
        '
        Me.HPDown2.Location = New System.Drawing.Point(2, 40)
        Me.HPDown2.Name = "HPDown2"
        Me.HPDown2.Size = New System.Drawing.Size(13, 23)
        Me.HPDown2.TabIndex = 7
        Me.HPDown2.Text = "v"
        Me.HPDown2.UseVisualStyleBackColor = True
        '
        'PCT22
        '
        Me.PCT22.Location = New System.Drawing.Point(151, 94)
        Me.PCT22.Multiline = True
        Me.PCT22.Name = "PCT22"
        Me.PCT22.Size = New System.Drawing.Size(146, 65)
        Me.PCT22.TabIndex = 6
        '
        'PCT21
        '
        Me.PCT21.Location = New System.Drawing.Point(6, 94)
        Me.PCT21.Multiline = True
        Me.PCT21.Name = "PCT21"
        Me.PCT21.Size = New System.Drawing.Size(139, 65)
        Me.PCT21.TabIndex = 5
        '
        'PCL22
        '
        Me.PCL22.AutoSize = True
        Me.PCL22.Location = New System.Drawing.Point(148, 77)
        Me.PCL22.Name = "PCL22"
        Me.PCL22.Size = New System.Drawing.Size(51, 13)
        Me.PCL22.TabIndex = 4
        Me.PCL22.Text = "Inventory"
        '
        'PCL21
        '
        Me.PCL21.AutoSize = True
        Me.PCL21.Location = New System.Drawing.Point(2, 77)
        Me.PCL21.Name = "PCL21"
        Me.PCL21.Size = New System.Drawing.Size(72, 13)
        Me.PCL21.TabIndex = 3
        Me.PCL21.Text = "Status effects"
        '
        'PHP2
        '
        Me.PHP2.AutoSize = True
        Me.PHP2.Location = New System.Drawing.Point(7, 16)
        Me.PHP2.Name = "PHP2"
        Me.PHP2.Size = New System.Drawing.Size(38, 13)
        Me.PHP2.TabIndex = 2
        Me.PHP2.Text = "Health"
        '
        'PCBar2
        '
        Me.PCBar2.Location = New System.Drawing.Point(16, 41)
        Me.PCBar2.Name = "PCBar2"
        Me.PCBar2.Size = New System.Drawing.Size(267, 20)
        Me.PCBar2.TabIndex = 1
        Me.PCBar2.Value = 100
        '
        'PCBox3
        '
        Me.PCBox3.Controls.Add(Me.HPUp3)
        Me.PCBox3.Controls.Add(Me.HPDown3)
        Me.PCBox3.Controls.Add(Me.PCT32)
        Me.PCBox3.Controls.Add(Me.PCT31)
        Me.PCBox3.Controls.Add(Me.PCL32)
        Me.PCBox3.Controls.Add(Me.PCL31)
        Me.PCBox3.Controls.Add(Me.PHP3)
        Me.PCBox3.Controls.Add(Me.PCBar3)
        Me.PCBox3.Location = New System.Drawing.Point(347, 2)
        Me.PCBox3.Name = "PCBox3"
        Me.PCBox3.Size = New System.Drawing.Size(303, 165)
        Me.PCBox3.TabIndex = 9
        Me.PCBox3.TabStop = False
        Me.PCBox3.Text = "PC3"
        '
        'HPUp3
        '
        Me.HPUp3.Location = New System.Drawing.Point(285, 39)
        Me.HPUp3.Name = "HPUp3"
        Me.HPUp3.Size = New System.Drawing.Size(13, 23)
        Me.HPUp3.TabIndex = 8
        Me.HPUp3.Text = "^"
        Me.HPUp3.UseVisualStyleBackColor = True
        '
        'HPDown3
        '
        Me.HPDown3.Location = New System.Drawing.Point(2, 40)
        Me.HPDown3.Name = "HPDown3"
        Me.HPDown3.Size = New System.Drawing.Size(13, 23)
        Me.HPDown3.TabIndex = 7
        Me.HPDown3.Text = "v"
        Me.HPDown3.UseVisualStyleBackColor = True
        '
        'PCT32
        '
        Me.PCT32.Location = New System.Drawing.Point(151, 94)
        Me.PCT32.Multiline = True
        Me.PCT32.Name = "PCT32"
        Me.PCT32.Size = New System.Drawing.Size(146, 65)
        Me.PCT32.TabIndex = 6
        '
        'PCT31
        '
        Me.PCT31.Location = New System.Drawing.Point(6, 94)
        Me.PCT31.Multiline = True
        Me.PCT31.Name = "PCT31"
        Me.PCT31.Size = New System.Drawing.Size(139, 65)
        Me.PCT31.TabIndex = 5
        '
        'PCL32
        '
        Me.PCL32.AutoSize = True
        Me.PCL32.Location = New System.Drawing.Point(148, 77)
        Me.PCL32.Name = "PCL32"
        Me.PCL32.Size = New System.Drawing.Size(51, 13)
        Me.PCL32.TabIndex = 4
        Me.PCL32.Text = "Inventory"
        '
        'PCL31
        '
        Me.PCL31.AutoSize = True
        Me.PCL31.Location = New System.Drawing.Point(2, 77)
        Me.PCL31.Name = "PCL31"
        Me.PCL31.Size = New System.Drawing.Size(72, 13)
        Me.PCL31.TabIndex = 3
        Me.PCL31.Text = "Status effects"
        '
        'PHP3
        '
        Me.PHP3.AutoSize = True
        Me.PHP3.Location = New System.Drawing.Point(7, 16)
        Me.PHP3.Name = "PHP3"
        Me.PHP3.Size = New System.Drawing.Size(38, 13)
        Me.PHP3.TabIndex = 2
        Me.PHP3.Text = "Health"
        '
        'PCBar3
        '
        Me.PCBar3.Location = New System.Drawing.Point(16, 41)
        Me.PCBar3.Name = "PCBar3"
        Me.PCBar3.Size = New System.Drawing.Size(267, 20)
        Me.PCBar3.TabIndex = 1
        Me.PCBar3.Value = 100
        '
        'PCBox4
        '
        Me.PCBox4.Controls.Add(Me.HPUp4)
        Me.PCBox4.Controls.Add(Me.HPDown4)
        Me.PCBox4.Controls.Add(Me.PCT42)
        Me.PCBox4.Controls.Add(Me.PCT41)
        Me.PCBox4.Controls.Add(Me.PCL42)
        Me.PCBox4.Controls.Add(Me.PCL41)
        Me.PCBox4.Controls.Add(Me.PHP4)
        Me.PCBox4.Controls.Add(Me.PCBar4)
        Me.PCBox4.Location = New System.Drawing.Point(347, 195)
        Me.PCBox4.Name = "PCBox4"
        Me.PCBox4.Size = New System.Drawing.Size(303, 165)
        Me.PCBox4.TabIndex = 9
        Me.PCBox4.TabStop = False
        Me.PCBox4.Text = "PC4"
        '
        'HPUp4
        '
        Me.HPUp4.Location = New System.Drawing.Point(285, 39)
        Me.HPUp4.Name = "HPUp4"
        Me.HPUp4.Size = New System.Drawing.Size(13, 23)
        Me.HPUp4.TabIndex = 8
        Me.HPUp4.Text = "^"
        Me.HPUp4.UseVisualStyleBackColor = True
        '
        'HPDown4
        '
        Me.HPDown4.Location = New System.Drawing.Point(2, 40)
        Me.HPDown4.Name = "HPDown4"
        Me.HPDown4.Size = New System.Drawing.Size(13, 23)
        Me.HPDown4.TabIndex = 7
        Me.HPDown4.Text = "v"
        Me.HPDown4.UseVisualStyleBackColor = True
        '
        'PCT42
        '
        Me.PCT42.Location = New System.Drawing.Point(151, 94)
        Me.PCT42.Multiline = True
        Me.PCT42.Name = "PCT42"
        Me.PCT42.Size = New System.Drawing.Size(146, 65)
        Me.PCT42.TabIndex = 6
        '
        'PCT41
        '
        Me.PCT41.Location = New System.Drawing.Point(6, 94)
        Me.PCT41.Multiline = True
        Me.PCT41.Name = "PCT41"
        Me.PCT41.Size = New System.Drawing.Size(139, 65)
        Me.PCT41.TabIndex = 5
        '
        'PCL42
        '
        Me.PCL42.AutoSize = True
        Me.PCL42.Location = New System.Drawing.Point(148, 77)
        Me.PCL42.Name = "PCL42"
        Me.PCL42.Size = New System.Drawing.Size(51, 13)
        Me.PCL42.TabIndex = 4
        Me.PCL42.Text = "Inventory"
        '
        'PCL41
        '
        Me.PCL41.AutoSize = True
        Me.PCL41.Location = New System.Drawing.Point(2, 77)
        Me.PCL41.Name = "PCL41"
        Me.PCL41.Size = New System.Drawing.Size(72, 13)
        Me.PCL41.TabIndex = 3
        Me.PCL41.Text = "Status effects"
        '
        'PHP4
        '
        Me.PHP4.AutoSize = True
        Me.PHP4.Location = New System.Drawing.Point(7, 16)
        Me.PHP4.Name = "PHP4"
        Me.PHP4.Size = New System.Drawing.Size(38, 13)
        Me.PHP4.TabIndex = 2
        Me.PHP4.Text = "Health"
        '
        'PCBar4
        '
        Me.PCBar4.Location = New System.Drawing.Point(16, 41)
        Me.PCBar4.Name = "PCBar4"
        Me.PCBar4.Size = New System.Drawing.Size(267, 20)
        Me.PCBar4.TabIndex = 1
        Me.PCBar4.Value = 100
        '
        'JournalButton
        '
        Me.JournalButton.Location = New System.Drawing.Point(368, 173)
        Me.JournalButton.Name = "JournalButton"
        Me.JournalButton.Size = New System.Drawing.Size(75, 23)
        Me.JournalButton.TabIndex = 10
        Me.JournalButton.Text = "Journal"
        Me.JournalButton.UseVisualStyleBackColor = True
        '
        'MapButton
        '
        Me.MapButton.Location = New System.Drawing.Point(287, 173)
        Me.MapButton.Name = "MapButton"
        Me.MapButton.Size = New System.Drawing.Size(75, 23)
        Me.MapButton.TabIndex = 11
        Me.MapButton.Text = "Map"
        Me.MapButton.UseVisualStyleBackColor = True
        '
        'RelationshipButton
        '
        Me.RelationshipButton.Location = New System.Drawing.Point(206, 173)
        Me.RelationshipButton.Name = "RelationshipButton"
        Me.RelationshipButton.Size = New System.Drawing.Size(75, 23)
        Me.RelationshipButton.TabIndex = 12
        Me.RelationshipButton.Text = "Relationships"
        Me.RelationshipButton.UseVisualStyleBackColor = True
        '
        'Form3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(649, 359)
        Me.Controls.Add(Me.RelationshipButton)
        Me.Controls.Add(Me.MapButton)
        Me.Controls.Add(Me.JournalButton)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.PCBox3)
        Me.Controls.Add(Me.PCBox4)
        Me.Controls.Add(Me.PCBox1)
        Me.Name = "Form3"
        Me.Text = "Form3"
        Me.PCBox1.ResumeLayout(False)
        Me.PCBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.PCBox3.ResumeLayout(False)
        Me.PCBox3.PerformLayout()
        Me.PCBox4.ResumeLayout(False)
        Me.PCBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PCBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PCL12 As System.Windows.Forms.Label
    Friend WithEvents PCL11 As System.Windows.Forms.Label
    Friend WithEvents PHP1 As System.Windows.Forms.Label
    Friend WithEvents PCBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents PCT12 As System.Windows.Forms.TextBox
    Friend WithEvents PCT11 As System.Windows.Forms.TextBox
    Friend WithEvents HPUp1 As System.Windows.Forms.Button
    Friend WithEvents HPDown1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents HPUp2 As System.Windows.Forms.Button
    Friend WithEvents HPDown2 As System.Windows.Forms.Button
    Friend WithEvents PCT22 As System.Windows.Forms.TextBox
    Friend WithEvents PCT21 As System.Windows.Forms.TextBox
    Friend WithEvents PCL22 As System.Windows.Forms.Label
    Friend WithEvents PCL21 As System.Windows.Forms.Label
    Friend WithEvents PHP2 As System.Windows.Forms.Label
    Friend WithEvents PCBar2 As System.Windows.Forms.ProgressBar
    Friend WithEvents PCBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents HPUp3 As System.Windows.Forms.Button
    Friend WithEvents HPDown3 As System.Windows.Forms.Button
    Friend WithEvents PCT32 As System.Windows.Forms.TextBox
    Friend WithEvents PCT31 As System.Windows.Forms.TextBox
    Friend WithEvents PCL32 As System.Windows.Forms.Label
    Friend WithEvents PCL31 As System.Windows.Forms.Label
    Friend WithEvents PHP3 As System.Windows.Forms.Label
    Friend WithEvents PCBar3 As System.Windows.Forms.ProgressBar
    Friend WithEvents PCBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents HPUp4 As System.Windows.Forms.Button
    Friend WithEvents HPDown4 As System.Windows.Forms.Button
    Friend WithEvents PCT42 As System.Windows.Forms.TextBox
    Friend WithEvents PCT41 As System.Windows.Forms.TextBox
    Friend WithEvents PCL42 As System.Windows.Forms.Label
    Friend WithEvents PCL41 As System.Windows.Forms.Label
    Friend WithEvents PHP4 As System.Windows.Forms.Label
    Friend WithEvents PCBar4 As System.Windows.Forms.ProgressBar
    Friend WithEvents JournalButton As System.Windows.Forms.Button
    Friend WithEvents MapButton As System.Windows.Forms.Button
    Friend WithEvents RelationshipButton As System.Windows.Forms.Button
End Class
