﻿Public Class Form4
    Dim newname As String
    Dim checkpaint As Boolean
    Dim drag As Boolean
    Dim coloured As Boolean
    Dim connects As New List(Of Label)
    Dim connectfinal As New List(Of Label)
    Dim penhistory As New List(Of Integer)
    Dim a As Integer
    Dim connectors As Integer
    Dim pename As String
    Dim con1 As Label
    Dim con2 As Label
    Private Sub Form4_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
    End Sub
    Public Sub connecterise(ByVal p1 As Point, ByVal p2 As Point, e As PaintEventArgs)
    End Sub
    Public Sub DrawLinePoint(ByVal e As PaintEventArgs)
        ' Create pen colours based on user choice
        Dim thePen As New Pen(Color.Green, 3)
        If RelType.Text = "Friend" Then
            thePen.Color = Color.Green
        ElseIf RelType.Text = "Enemy" Then
            thePen.Color = Color.Red
        ElseIf RelType.Text = "Memer" Then
            thePen.Color = Color.Black
        End If
        If connects.Count = 2 Then
            'records the currently selected labels and resets their colour, also ensures that there isn't a previously selected two labels and records the pen's colour
            connectfinal.Add(connects(0))
            connectfinal.Add(connects(1))
            connects(0).BackColor = DefaultBackColor
            connects(1).BackColor = DefaultBackColor
            If coloured = False Then
                If thePen.Color = Color.Green Then
                    penhistory.Add(0)
                    coloured = True
                ElseIf thePen.Color = Color.Red Then
                    penhistory.Add(1)
                    coloured = True
                ElseIf thePen.Color = Color.Black Then
                    penhistory.Add(2)
                    coloured = True
                End If
            End If
            connects.Clear()
            connectors += 1
        End If
        'thePen.Dispose()
        For i = 1 To connectfinal.Count
            If i Mod 2 <> 0 Then
                'the colour of the pen is recalled via the integer value of the currently recalled penhistory value
                If penhistory((i / 2) - 1) = 0 Then
                    thePen.Color = Color.Green
                ElseIf penhistory((i / 2) - 1) = 1 Then
                    thePen.Color = Color.Red
                ElseIf penhistory((i / 2) - 1) = 2 Then
                    thePen.Color = Color.Black
                End If
                e.Graphics.DrawLine(thePen, connectfinal(i).Location, connectfinal(i - 1).Location)
            End If
        Next
        thePen.Dispose()
    End Sub

    Private Sub Form4_Paint(sender As Object, e As PaintEventArgs) Handles Me.Paint
        'ensures that lines are drawable, then runs the initial paint code
        If checkpaint = True Then
            DrawLinePoint(e)
        End If
    End Sub

    Private Sub Form2_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown
        'Button3.Location = New Point(MousePosition.X, MousePosition.Y)
        For i = 1 To a
            If Me.Controls("Marker" & (i - 1)).Bounds.Contains(Form.MousePosition) Then
                If connects.Contains(Me.Controls("Marker" & (i - 1))) = False Then
                    connects.Add(Me.Controls("Marker" & (i - 1)))
                    Me.Controls("Marker" & (i - 1)).BackColor = Color.DarkRed
                    coloured = False
                End If
            End If
        Next
        drag = True
    End Sub
    Private Sub Form2_MouseUp(sender As Object, e As MouseEventArgs) Handles Me.MouseUp
        'ensures dragging does not occur while the user has the mouse up, and performs a final refresh of all drawn lines
        drag = False
        Refresh()
    End Sub

    Private Sub NewMarker_Click(sender As Object, e As EventArgs) Handles NewMarker.Click
        'simililarly for form2, this code generates a label and allows for the inputting of customly created labels
        newname = InputBox("Enter the name of the new marker", "newmarker", "")
        Dim kek As New Label
        kek.Name = "Marker" & a
        kek.Text = newname
        kek.Size = New Size(75, 50)
        kek.Font = New Drawing.Font("Copperplate Gothic", 12)
        kek.Location = New Point(100, 100)
        Me.Controls.Add(kek)
        a += 1
        checkpaint = True
        Refresh()
    End Sub
    Private Sub Form2_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
        'checks whether the user is currently dragging
        If drag = True Then
            'cycles through all of the markers existing
            For i = 1 To a
                'checks if the user is inside the boundary of the markers
                If Me.Controls("Marker" & (i - 1)).Bounds.Contains(Form.MousePosition) Then
                    If drag = True Then
                        'modifies the position of the label to be equal to the mouse's position
                        Me.Controls("Marker" & (i - 1)).Left += (MousePosition.X - Me.Controls("Marker" & (i - 1)).Left - 25)
                        Me.Controls("Marker" & (i - 1)).Top += (MousePosition.Y - Me.Controls("Marker" & (i - 1)).Top - 25)
                        Refresh()
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles PartyButton.Click
        Me.Hide()
        Form3.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles JournalButton.Click
        Me.Hide()
        Form1.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles MapButton.Click
        Me.Hide()
        Form2.Show()
    End Sub
End Class