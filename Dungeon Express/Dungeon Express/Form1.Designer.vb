﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.EntryBox = New System.Windows.Forms.RichTextBox()
        Me.LogBox = New System.Windows.Forms.RichTextBox()
        Me.RollButton = New System.Windows.Forms.Button()
        Me.DiceBox = New System.Windows.Forms.RichTextBox()
        Me.MapBox = New System.Windows.Forms.Button()
        Me.LogCombo = New System.Windows.Forms.ComboBox()
        Me.PartyBox = New System.Windows.Forms.Button()
        Me.DiceCombo = New System.Windows.Forms.ComboBox()
        Me.RelationshipBox = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'EntryBox
        '
        Me.EntryBox.Location = New System.Drawing.Point(158, 245)
        Me.EntryBox.Name = "EntryBox"
        Me.EntryBox.Size = New System.Drawing.Size(263, 21)
        Me.EntryBox.TabIndex = 0
        Me.EntryBox.Text = ""
        '
        'LogBox
        '
        Me.LogBox.Location = New System.Drawing.Point(93, 25)
        Me.LogBox.Name = "LogBox"
        Me.LogBox.ReadOnly = True
        Me.LogBox.RightMargin = 10000
        Me.LogBox.Size = New System.Drawing.Size(328, 214)
        Me.LogBox.TabIndex = 1
        Me.LogBox.Text = ""
        '
        'RollButton
        '
        Me.RollButton.Location = New System.Drawing.Point(489, 244)
        Me.RollButton.Name = "RollButton"
        Me.RollButton.Size = New System.Drawing.Size(251, 21)
        Me.RollButton.TabIndex = 2
        Me.RollButton.Text = "roll"
        Me.RollButton.UseVisualStyleBackColor = True
        '
        'DiceBox
        '
        Me.DiceBox.Location = New System.Drawing.Point(427, 25)
        Me.DiceBox.Name = "DiceBox"
        Me.DiceBox.ReadOnly = True
        Me.DiceBox.Size = New System.Drawing.Size(313, 217)
        Me.DiceBox.TabIndex = 4
        Me.DiceBox.Text = ""
        '
        'MapBox
        '
        Me.MapBox.Location = New System.Drawing.Point(746, 114)
        Me.MapBox.Name = "MapBox"
        Me.MapBox.Size = New System.Drawing.Size(75, 23)
        Me.MapBox.TabIndex = 5
        Me.MapBox.Text = "map"
        Me.MapBox.UseVisualStyleBackColor = True
        '
        'LogCombo
        '
        Me.LogCombo.FormattingEnabled = True
        Me.LogCombo.Items.AddRange(New Object() {"General", "Story", "PC", "NPC", "OOC", "Quest"})
        Me.LogCombo.Location = New System.Drawing.Point(93, 245)
        Me.LogCombo.Name = "LogCombo"
        Me.LogCombo.Size = New System.Drawing.Size(59, 21)
        Me.LogCombo.TabIndex = 6
        Me.LogCombo.Text = "General"
        '
        'PartyBox
        '
        Me.PartyBox.Location = New System.Drawing.Point(746, 172)
        Me.PartyBox.Name = "PartyBox"
        Me.PartyBox.Size = New System.Drawing.Size(75, 23)
        Me.PartyBox.TabIndex = 7
        Me.PartyBox.Text = "Party"
        Me.PartyBox.UseVisualStyleBackColor = True
        '
        'DiceCombo
        '
        Me.DiceCombo.FormattingEnabled = True
        Me.DiceCombo.Items.AddRange(New Object() {"D3", "D6", "D12", "D20"})
        Me.DiceCombo.Location = New System.Drawing.Point(427, 245)
        Me.DiceCombo.Name = "DiceCombo"
        Me.DiceCombo.Size = New System.Drawing.Size(56, 21)
        Me.DiceCombo.TabIndex = 8
        Me.DiceCombo.Text = "D6"
        '
        'RelationshipBox
        '
        Me.RelationshipBox.Location = New System.Drawing.Point(746, 143)
        Me.RelationshipBox.Name = "RelationshipBox"
        Me.RelationshipBox.Size = New System.Drawing.Size(75, 23)
        Me.RelationshipBox.TabIndex = 10
        Me.RelationshipBox.Text = "relationships"
        Me.RelationshipBox.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(848, 293)
        Me.Controls.Add(Me.RelationshipBox)
        Me.Controls.Add(Me.DiceCombo)
        Me.Controls.Add(Me.PartyBox)
        Me.Controls.Add(Me.LogCombo)
        Me.Controls.Add(Me.MapBox)
        Me.Controls.Add(Me.DiceBox)
        Me.Controls.Add(Me.RollButton)
        Me.Controls.Add(Me.LogBox)
        Me.Controls.Add(Me.EntryBox)
        Me.KeyPreview = True
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EntryBox As System.Windows.Forms.RichTextBox
    Friend WithEvents LogBox As System.Windows.Forms.RichTextBox
    Friend WithEvents RollButton As System.Windows.Forms.Button
    Friend WithEvents DiceBox As System.Windows.Forms.RichTextBox
    Friend WithEvents MapBox As System.Windows.Forms.Button
    Friend WithEvents LogCombo As System.Windows.Forms.ComboBox
    Friend WithEvents PartyBox As System.Windows.Forms.Button
    Friend WithEvents DiceCombo As System.Windows.Forms.ComboBox
    Friend WithEvents RelationshipBox As System.Windows.Forms.Button

End Class
