﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
    End Sub
    Private Sub EntryBox_MouseClick(sender As Object, e As MouseEventArgs) Handles EntryBox.MouseClick
    End Sub
    Private Sub Form1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        'checks if the enter key is pressed
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            'checks then if the box is the active control
            If EntryBox.Name = Me.ActiveControl.Name Then
                'appends a new line with what the comboBox has inside it
                LogBox.AppendText("<" + LogCombo.Text + ">" + " - " + EntryBox.Text)
                LogBox.Select(LogBox.TextLength, 0)
                LogBox.ScrollToCaret()
                EntryBox.Text = ""
                'resets the line to be ready to recieve a new entry
            End If
        End If
    End Sub
    Public Function GetRandom(ByVal Min As Integer, ByVal Max As Integer) As Integer
        '-------------------------------------------------------------------------'
        ''I TOOK THIS FUNCTION FROM: stackoverflow.com/questions/18676/random-int-in-vb-net thanks to M.Jaame for the link
        ' by making Generator static, we preserve the same instance '
        ' (i.e., do not create new instances with the same seed over and over) '
        ' between calls '
        Static Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max)
    End Function
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles RollButton.Click
        'spaghetti code that checks what the dice combo box has in it and then produces a random number based on this, alongside a message
        If DiceCombo.Text = "D6" Then
            DiceBox.AppendText("You rolled a D6, getting " & Str(GetRandom(1, 7)) & Environment.NewLine)
        ElseIf DiceCombo.Text = "D3" Then
            DiceBox.AppendText("You rolled a D3, getting " & Str(GetRandom(1, 4)) & Environment.NewLine)
        ElseIf DiceCombo.Text = "D12" Then
            DiceBox.AppendText("You rolled a D12, getting " & Str(GetRandom(1, 13)) & Environment.NewLine)
        ElseIf DiceCombo.Text = "D20" Then
            DiceBox.AppendText("You rolled a D20, getting " & Str(GetRandom(1, 21)) & Environment.NewLine)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles MapBox.Click
        Form2.Show()
        Me.Hide()
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles PartyBox.Click
        Form3.Show()
        Me.Hide()
    End Sub
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles RelationshipBox.Click
        Me.Hide()
        Form4.Show()
    End Sub
End Class
